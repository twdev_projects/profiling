# Profiling data collection

This repo demonstrates an approach to collecting profile data using scoped probes.
The data is then visualised using chrome's `about:tracing`.

You can read more about Chrome's tracing tools [here](https://www.chromium.org/developers/how-tos/trace-event-profiling-tool/).

## Building

    meson setup --buildtype debugoptimized bld

    meson configure -Dstrip=false bld

    meson compile -C bld

This will produce a test executable `profiling`.  The test
program will be instrumented and will produce a json output compatible with
[trace event](https://docs.google.com/document/d/1CvAClvFfyA5R-PhYUmn5OOQtYMH4h6I0nSsKchNAySU/edit#heading=h.yr4qxyxotyw) requirements.

## Collecting profiling data

Run the test executable:

    ./bld/profiling >data.json

To visualise the data open `data.json` with [chrome tracing](about:tracing).

## Other branches in this repo

- [exp/cyg]() - demonstrates an approach with a library that uses gcc's function
  instrumentation to inject probes automatically.
