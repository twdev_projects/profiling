#include <unistd.h>

#include <iostream>
#include <nlohmann/json.hpp>
#include <sstream>
#include <thread>
#include <timer.hpp>
#include <vector>

using json = nlohmann::json;

extern "C" {

void __cyg_profile_func_enter(void *this_fn, void *call_site) {}

void __cyg_profile_func_exit(void *this_fn, void *call_site) {}
}

json events;

std::string getTid() {
  std::stringstream ss;
  auto tid = std::this_thread::get_id();
  ss << tid;
  return ss.str();
}

template <typename TimePointT>
void emitEvent(std::string name, std::string phase, TimePointT ts) {
  using namespace std::chrono;

  // some hard-coded data
  std::string category = "PERF";

  json j;
  j["cat"] = category;
  j["name"] = name;
  j["ph"] = phase;
  j["pid"] = ::getpid();
  j["tid"] = getTid();
  j["ts"] = duration_cast<microseconds>(ts.time_since_epoch()).count();

  events.push_back(j);
}

#define SCOPED_PROFILE()                                                   \
  ScopedProbe<std::chrono::high_resolution_clock> st_##__func__##__LINE__( \
      __func__, [](auto n, auto ts) { emitEvent(n, "B", ts); },            \
      [](auto n, auto ts) { emitEvent(n, "E", ts); })

void func1() {
  SCOPED_PROFILE();
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
}

void func2() {
  SCOPED_PROFILE();
  std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

void func3() {
  SCOPED_PROFILE();
  func1();
  func2();
}

int main() {
  {
    SCOPED_PROFILE();
    func3();
  }

  std::cout << events.dump() << std::endl;
  return 0;
};
